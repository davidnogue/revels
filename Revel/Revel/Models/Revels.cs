﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Revel.Models
{
    public class Revels
    {
        public String Nombre { get; set; }

        public String Planeta { get; set; }

        public void AddRevel() {

            string path = @"D:\Revel\Revel\revel-list.txt";
            if (!File.Exists(path))
            {
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine("Revel List");
                }
            }

            // This text is always added, making the file longer over time
            // if it is not deleted.
            using (StreamWriter sw = File.AppendText(path))
            {
                DateTime localDate = DateTime.Now;
                sw.WriteLine("rebel {0} on {1} at {2}",this.Nombre, this.Planeta,localDate);
            }

        }
    }
}