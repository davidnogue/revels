﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Revel.Models;
using System.IO;


namespace Revel.Controllers
{
    public class RevelController : ApiController
    {
        List<Revels> revelList = new List<Revels>();
        public List<Revels> GetRevels() {
            return this.revelList;
        }
        public bool Post([FromBody] List<String> revel)
        {
            try
            {
                if(revel.Count != 2)
                {
                    Exception ex = new Exception(string.Format("Send two strings only"));
                    throw ex;
                }
                Revels rev = new Revels { Nombre = revel[0], Planeta = revel[1] };
                rev.AddRevel();
            }
            catch (NullReferenceException  e)
            {
                string path = @"D:\Revel\Revel\error-log.txt";
                if (!File.Exists(path))
                {
                    // Create a file to write to.
                    using (StreamWriter sw = File.CreateText(path))
                    {
                        sw.WriteLine("Errorlog");
                    }
                }

                // This text is always added, making the file longer over time
                // if it is not deleted.
                using (StreamWriter sw = File.AppendText(path))
                {
                    DateTime localDate = DateTime.Now;
                    sw.WriteLine("{0}", localDate);
                    sw.WriteLine("Exception source: {0}", e);
                }
                return false;
            }
            catch (ArgumentOutOfRangeException  e)
            {
                string path = @"D:\Revel\Revel\error-log.txt";
                if (!File.Exists(path))
                {
                    // Create a file to write to.
                    using (StreamWriter sw = File.CreateText(path))
                    {
                        sw.WriteLine("Errorlog");
                    }
                }

                // This text is always added, making the file longer over time
                // if it is not deleted.
                using (StreamWriter sw = File.AppendText(path))
                {
                    DateTime localDate = DateTime.Now;
                    sw.WriteLine("{0}", localDate);
                    sw.WriteLine("Exception source: {0}", e.TargetSite);
                    sw.WriteLine("{0}", e.Message);

                }
                return false;
            }
            catch (Exception e)
            {
                string path = @"D:\Revel\Revel\error-log.txt";
                if (!File.Exists(path))
                {
                    // Create a file to write to.
                    using (StreamWriter sw = File.CreateText(path))
                    {
                        sw.WriteLine("Errorlog");
                    }
                }

                // This text is always added, making the file longer over time
                // if it is not deleted.
                using (StreamWriter sw = File.AppendText(path))
                {
                    DateTime localDate = DateTime.Now;
                    sw.WriteLine("{0}", localDate);
                    sw.WriteLine("Exception source: {0}", e.TargetSite);
                    sw.WriteLine("{0}", e.Message);
                }
                return false;
            }

            return true;
        }

    }
}
