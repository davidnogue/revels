﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Revel.Controllers;
using Revel.Models;

namespace UnitTestRevel
{
    [TestClass]
    public class Reveltest
    {
        [TestMethod]
        public void TestAddRevel()
        {
            List<String> revelde = new List<String>();
            revelde.Add("Han");
            revelde.Add("Tatuin");
            RevelController controler = new RevelController();

            bool result = controler.Post(revelde);

            Assert.IsTrue(result);
        }
    }
}
